export declare type GameInputEventType = 'press' | 'release';
export declare type GameInputEventListener = (name: string) => void;
export declare const config: {
    iOSHacks: boolean;
};
export declare const keyStates: boolean[];
/**
 * Add the global key listeners. Required to handle keyboard events.
 */
export declare function addKeyListeners(): void;
/**
 * Remove the global key listeners
 */
export declare function removeKeyListeners(): void;
export declare function listeningKeys(): boolean;
/**
 * Creates an input object associated with key code(s) and element(s)
 * that can be queried for state and listened to for press/release events.
 */
export declare function create(name: string, keyCodes?: number[], elements?: ArrayLike<Element>): void;
/**
 * Gets the pressed/not-pressed state of the named input
 */
export declare function get(name: string): boolean;
/**
 * Attach an input listener callback.
 */
export declare function addListener(name: string, type: GameInputEventType, callback: GameInputEventListener): void;
/**
 * Detach an input listener callback.
 */
export declare function removeListener(name: string, type: GameInputEventType, callback: GameInputEventListener): void;
export declare function snuffEvents(el: Element): void;
/**
 * iOS Hack utility - prevents events on the given element
 */
export declare function snuffiOSEvents(el: Element): void;
