// Module to handle keyboard, mouse, touch events.
// Useful for games.
"use strict";
var DEVICE_NONE = 0;
var DEVICE_MOUSE = 1;
var DEVICE_TOUCH = 2;
var isIOS = !!navigator.userAgent.match(/iPhone|iPad|iPod/i);
var inputs = Object.create(null);
exports.config = {
    iOSHacks: true
};
exports.keyStates = (function () {
    var k = new Array(256);
    for (var i = 0; i < 256; ++i)
        k[i] = false;
    return k;
}());
var keyListenersInitialized = false;
/**
 * Add the global key listeners. Required to handle keyboard events.
 */
function addKeyListeners() {
    if (keyListenersInitialized) {
        console.warn("Key listeners already added.");
        return;
    }
    document.addEventListener('keydown', onKeyDown, true);
    document.addEventListener('keyup', onKeyUp, true);
    keyListenersInitialized = true;
}
exports.addKeyListeners = addKeyListeners;
/**
 * Remove the global key listeners
 */
function removeKeyListeners() {
    if (!keyListenersInitialized) {
        console.warn("Key listeners were not yet added.");
        return;
    }
    document.removeEventListener('keydown', onKeyDown, true);
    document.removeEventListener('keyup', onKeyUp, true);
    keyListenersInitialized = false;
}
exports.removeKeyListeners = removeKeyListeners;
function listeningKeys() {
    return keyListenersInitialized;
}
exports.listeningKeys = listeningKeys;
function onKeyDown(e) {
    var code = e.keyCode;
    if (!exports.keyStates[code]) {
        // this key state changed
        exports.keyStates[code] = true;
        // See if we need to update state of any inputs
        var inps = findInputsWithKeyCode(code);
        for (var i = 0, n = inps.length; i < n; ++i) {
            var input = inps[i];
            if (!input.pressed) {
                input.pressed = true;
                for (var j = 0, m = input.listeners.length; j < m; ++j) {
                    var l = input.listeners[j];
                    if (l.type === 'press')
                        l.callback(input.name);
                }
            }
        }
    }
}
function onKeyUp(e) {
    var code = e.keyCode;
    if (exports.keyStates[code]) {
        // this key state changed
        exports.keyStates[code] = false;
        // See if we need to update state of any inputs
        var inps = findInputsWithKeyCode(code);
        for (var i = 0, n = inps.length; i < n; ++i) {
            var input = inps[i];
            if (input.pressed) {
                // Check if any other keys are down
                if (inputKeyState(input.name))
                    return;
                // Check if any elements are pressed
                if (inputElementState(input.name))
                    return;
                // No - so this input is truly released
                input.pressed = false;
                for (var j = 0, m = input.listeners.length; j < m; ++j) {
                    var l = input.listeners[j];
                    if (l.type === 'release')
                        l.callback(input.name);
                }
            }
        }
    }
}
/** Find a listener for the given input of the given type */
function findInputListener(input, type) {
    for (var i = 0; i < input.listeners.length; ++i) {
        var l = input.listeners[i];
        if (l.type === type) {
            return l;
        }
    }
}
/** Find input(s) with a key code */
function findInputsWithKeyCode(code) {
    var inps = [];
    for (var name_1 in inputs) {
        var input = inputs[name_1];
        var kcs = input.keyCodes;
        for (var i = 0, n = kcs.length; i < n; ++i) {
            if (kcs[i] === code) {
                inps.push(input);
            }
        }
    }
    return inps;
}
/** Find if a key associated with this input is pressed */
function inputKeyState(name) {
    var input = inputs[name];
    if (!input) {
        console.warn("Unrecognized input name:", name);
        return;
    }
    var kcs = input.keyCodes;
    for (var i = 0, n = kcs.length; i < n; ++i) {
        if (exports.keyStates[kcs[i]])
            return true;
    }
    return false;
}
/** Find if an element associated with this input is pressed */
function inputElementState(name) {
    var input = inputs[name];
    if (!input) {
        console.warn("Unrecognized input name:", name);
        return;
    }
    var els = input.elements;
    for (var i = 0, n = els.length; i < n; ++i) {
        if (els[i].pressed)
            return true;
    }
    return false;
}
function addElementListeners(gie, input) {
    var el = gie.element;
    el.addEventListener('touchstart', function () {
        pressElement(gie, input, DEVICE_TOUCH);
    });
    el.addEventListener('touchend', function () {
        releaseElement(gie, input, DEVICE_TOUCH);
    });
    el.addEventListener('mousedown', function () {
        pressElement(gie, input, DEVICE_MOUSE);
    });
    el.addEventListener('mouseup', function () {
        releaseElement(gie, input, DEVICE_MOUSE);
    });
    snuffEvents(el);
}
function pressElement(gie, input, device) {
    if (gie.device !== DEVICE_NONE && gie.device !== device)
        return;
    gie.device = device;
    gie.pressed = true;
    if (input.pressed)
        return;
    input.pressed = true;
    var l = findInputListener(input, 'press');
    l && l.callback(input.name);
}
function releaseElement(gie, input, device) {
    if (gie.device !== DEVICE_NONE && gie.device !== device)
        return;
    gie.pressed = false;
    setTimeout(function () {
        // iOS will fire a delayed mouse event after touchend.
        // Delaying the device reset will ignore that mouse event.
        gie.device = DEVICE_NONE;
    }, 500);
    // check keystate - if still pressed, exit - no event.
    if (!input.pressed || inputKeyState(name))
        return;
    input.pressed = false;
    var l = findInputListener(input, 'release');
    l && l.callback(input.name);
}
/**
 * Creates an input object associated with key code(s) and element(s)
 * that can be queried for state and listened to for press/release events.
 */
function create(name, keyCodes, elements) {
    if (!name || typeof name !== 'string') {
        throw new Error("Invalid name for input.");
    }
    if (name in inputs) {
        throw new Error("Input with name '" + name + " already exists.");
    }
    var input = {
        name: name,
        pressed: false,
        keyCodes: [],
        elements: [],
        listeners: []
    };
    if (keyCodes && keyCodes.length > 0) {
        if (!keyListenersInitialized) {
            addKeyListeners();
        }
        for (var i = 0; i < keyCodes.length; ++i) {
            var code = keyCodes[i];
            if (input.keyCodes.indexOf(code) >= 0) {
                console.warn("Duplicate key code (" + code + ") specified for input '" + name + "'");
            }
            input.keyCodes[i] = code;
        }
    }
    else if (!elements || elements.length < 1) {
        throw new Error("No keycodes or elements supplied to createInput");
    }
    if (elements && elements.length > 0) {
        for (var i = 0; i < elements.length; ++i) {
            var gie = { element: elements[i], pressed: false, device: 0 };
            input.elements.push(gie);
            addElementListeners(gie, input);
        }
    }
    inputs[name] = input;
}
exports.create = create;
/**
 * Gets the pressed/not-pressed state of the named input
 */
function get(name) {
    var i = inputs[name];
    return i ? i.pressed : false;
}
exports.get = get;
/**
 * Attach an input listener callback.
 */
function addListener(name, type, callback) {
    if (!name || typeof name !== 'string') {
        throw new Error("Invalid type for name.");
    }
    if (type !== 'press' && type !== 'release') {
        throw new Error("Invalid input event type.");
    }
    var input = inputs[name];
    if (!input) {
        throw new Error("Input with name '" + name + "' not found.");
    }
    var ls = input.listeners;
    for (var i = 0; i < ls.length; ++i) {
        var l = ls[i];
        if (l.type === type && l.callback === callback) {
            console.warn("Already added this listener.");
            return;
        }
    }
    ls.push({ type: type, callback: callback });
}
exports.addListener = addListener;
/**
 * Detach an input listener callback.
 */
function removeListener(name, type, callback) {
    var input = inputs[name];
    if (!input) {
        console.warn("Input not found with name " + name);
    }
    var ls = input.listeners;
    for (var i = ls.length - 1; i >= 0; --i) {
        var l = ls[i];
        if (l.type === type && l.callback === callback) {
            ls.splice(i, 1);
            return;
        }
    }
    console.warn("Listener not found for input '" + name + "' with type " + type + ", cannot remove.");
}
exports.removeListener = removeListener;
function snuffEvents(el) {
    el.addEventListener('touchmove', function (e) {
        // Prevent dragging of this element
        e.preventDefault();
    });
    snuffiOSEvents(el);
}
exports.snuffEvents = snuffEvents;
/** iOS troublesome events to prevent */
var IOS_SNUFF_EVENTS = ['dblclick'];
/**
 * iOS Hack utility - prevents events on the given element
 */
function snuffiOSEvents(el) {
    if (!isIOS || !exports.config.iOSHacks)
        return;
    IOS_SNUFF_EVENTS.forEach(function (name) {
        el.addEventListener(name, function (e) { e.preventDefault(); });
    });
}
exports.snuffiOSEvents = snuffiOSEvents;
