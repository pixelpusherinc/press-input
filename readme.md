# press-input

A utility for listening for and reading 'pressed' state of keyboard, mouse and touch inputs.

## Install

To add this to your `package.json` file:

	npm install -D git+https://bitbucket.org/pixelpusherinc/press-input.git

## The gist

```typescript
import * as input from 'input'

// Create an input that will observe the up arrow key,
// the W key and a button element
input.create('forward',
	[38, 87],
	[document.getElementById('button-forward')]
)

// Set up a callback that will be invoked when the key
// or button are pressed
function onPress (name: string) {
	console.log(name + " was pressed.")
}
input.addListener('forward', 'press', onPress)

// Or test the current state:
const isPressed = input.get('forward')

// Remove the callback
input.removeListener('forward', 'press', onPress)
```

In the above example, the state for the 'forward' input will be true if the button is currently depressed (by mouse our touch,) *or* if the up arrow key is pressed *or* if the W key is pressed.

'press' events will be fired only when the first trigger occurs. So if the up arrow is pressed, then the W key is pressed while the up arrow is still pressed, you will only get the first event. Similarly, you will only get the 'release' event when the last key/element is released.

Auto-repeating key press events will also be ignored.

## Testing keystates only

This utility can be used just to test keystates. Normally, a global keypress listener will be initialized when inputs are created having keycodes, however if you only want to test keystates, you must initialize the key listeners manually:

```typescript
input.addKeyListeners()
```

Then you can check the current state of any key state by its code:

```typescript
if (input.keyStates(38)) {
	console.log("Up arrow key is pressed.")
}
```

## Mobile hacks

Mobile touch input can have some undesirable effects, like dragging an image being used as a button. iOS will zoom in when elements are double-tapped. By default, this library will prevent those events for any input being listened to. If there are other elements you wish to disable but not listen for press events, you can just prevent those events:

```typescript
input.snuffEvents(document.getElementById('my-element'))
```
